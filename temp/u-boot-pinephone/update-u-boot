#!/bin/sh

verbose=
board=
device=
dryrun=
imagedir=
default_freq=528
freq=

get_boot_blockdev() {
	# Find the blockdevice where /boot is mounted from
	mount | sed -n -E 's:.*(/dev/mmcblk\d)p\d on /boot .*:\1:p'	
}

get_defaults() {
	if [ -z "$board" -a -e /sys/firmware/devicetree/base/compatible ]; then
		case "$(cat /sys/firmware/devicetree/base/compatible 2>/dev/null)" in
		wand,*) board=wand ;;
		pine64,pinebook-pro*) board=pinebookpro ;;
		pine64,rockpro64*) board=rockpro64 ;;
		pine64,pinephone*) board=pinephone ;;
		esac
	fi

	if [ -z "$device" ]; then
		case "$board" in
		wand|cubie|cubie2) device=/dev/mmcblk0p0 ;;
		mx6cuboxi) device=/dev/mmcblk0 ;;
		pinebookpro|rockpro64) device=/dev/mmcblk2 ;;
		pinephone) device=$(get_boot_blockdev) ;;
		esac
	fi

	if [ -z "$imagedir" ]; then
		imagedir="$(realpath $(dirname $0))"
		[ -f "$imagedir/README.txt" ] || imagedir="/usr/share/u-boot"
	fi

	if [ -z "$freq" -a -e /proc/device-tree/memory/ram_freq ]; then
		freq=$(cat /proc/device-tree/memory/ram_freq 2>/dev/null)
		echo Detected custom clock: $freq MHz
	elif [ -z "$freq" ]; then
		echo Using default clock: $default_freq MHz
	else
		echo Changing clock: $freq MHz
	fi
}

die() {
	echo "ERROR: $@"
	exit 1
}

usage() {
	get_defaults

	cat <<EOF
usage: $0 [-n,--dry-run] [-i,--imagedir <imagedir>] [-b|--board <board-type>] [-d|--device <device>] [-r|--ram-freq <freq>]

options:

 -b,--board <board>       Specify the board type: wand, cubie, cubie2, cuboxi, pinebookpro, rockpro64
                          (current default: ${board:-none})

 -d,--device <device>     Specify the device where to install u-boot
                          (current default: ${device:-none})

 -i,--imagedir <imagedir> Specify u-boot image directory
                          (current default: ${imagedir:-none})

 -n,--dry-run             Print commands but don't execute them

 -r,--ram-freq            RAM clock speed, one of:
                          * 528 (default for new postmarketOS installations)
                          * 552 (default before v21.03 / edge 2021-04-05)
                          * 624

EOF
}

while [ $# -gt 0 ]; do
	opt="$1"
	shift
	case "$opt" in
	-b|--board)
		case "$1" in
		wand|wandboard) board="wand" ;;
		cubie|cubieboard) board="cubie" ;;
		cuboxi|mx6cuboxi) board="mx6cuboxi" ;;
		pinebookpro) board="pinebookpro" ;;
		rockpro64) board="rockpro64" ;;
		pinephone) board="pinephone" ;;
		*) usage; exit 1;;
		esac
		shift
		;;
	-d|--device)
		device="$1"
		shift
		;;
	-i|--imagedir)
		imagedir="$1"
		shift
		;;
	-n|--dry-run)
		dryrun="echo"
		;;
	-r|--ram-freq)
		case "$1" in
		528) freq="528" ;;
		552) freq="552" ;;
		624) freq="624" ;;
		*) usage; exit 1;;
		esac
		shift
		;;
        --)
                break
                ;;
        -*)
                usage
                exit 1
                ;;
        esac
done

get_defaults
if [ -z "$board" -o -z "$device" -o -z "$imagedir" -o  ! -e "$imagedir" ]; then
	usage
	exit 1
fi

if [ -z "$dryrun" ]; then
	if [ ! -z "$freq" ]; then
		echo "/!\ Disclaimer warning:"
		echo "Overclocking your device may render it unstable or prevent it to boot"
		echo "If you're unable to boot your device you'll have to flash an image to a mSD card"
		echo "then boot on it and update u-boot running this command:"
		echo "update-u-boot -d /dev/mmcblk2"
		echo 
		read -p "Continue? [y/Y] " -n 1 -r
		echo 
		if [[ ! $REPLY =~ ^[Yy]$ ]]
		then
			exit 0
		fi
	else
		freq=$default_freq
	fi
	echo "Updating $board u-boot in $device in 3 seconds..."
	sleep 3
fi

(
set -e
case "$board" in
wand)
	[ -e "$imagedir/wandboard" ] || die "wandboard images not installed, apk add u-boot-wandboard"
	$dryrun dd if=$imagedir/wandboard/SPL of=$device bs=1k seek=1 status=none
	$dryrun dd if=$imagedir/wandboard/u-boot.img of=$device bs=1k seek=69 status=none
	;;
cubie|cubie2)
	[ -e "$imagedir/Cubieboard${board#cubie}" ] || die "Cubieboard images not installed, apk add u-boot-cubieboard"
	$dryrun dd if=$imagedir/Cubieboard${board#cubie}/u-boot-sunxi-with-spl.bin of=$device bs=1024 seek=8 status=none
	;;
mx6cuboxi)
	[ -e "$imagedir/mx6cuboxi" ] || die "iMX6 Cubox-i images not installed, apk add u-boot-cuboxi"
	$dryrun dd if=$imagedir/mx6cuboxi/SPL of=$device bs=1k seek=1 status=none
	$dryrun dd if=$imagedir/mx6cuboxi/u-boot.img of=$device bs=1k seek=69 status=none
	;;
pinebookpro|rockpro64)
	[ -e "$imagedir/pine64-rockpro64/" ] || die "rockpro64 images not installed, apk add u-boot-rockpro64"
	$dryrun dd if=$imagedir/pine64-rockpro64/u-boot-rockchip.bin of=$device bs=1024 seek=32 status=none
	;;
pinephone)
	[ -e "$imagedir/pine64-pinephone/" ] || die "pinephone images not installed, apk add u-boot-pinephone"
	$dryrun dd if=$imagedir/pine64-pinephone/u-boot-sunxi-with-spl-$freq.bin of=$device bs=1024 seek=8 status=none
	;;
esac
$dryrun sync
) || die "U-Boot installation in $device failed"

[ -z "$dryrun" ] && echo "Completed successfully."
